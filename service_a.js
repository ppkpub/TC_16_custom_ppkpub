/**
 * @author: ppkpub (ppkpub.org)  baibingyi(baiby@live.cn)
 * 2016-11-05
 * TechCrunch
 * 
 */ 

var http = require('http');
var request = require('./request');
var url = require('url');

http.createServer(function (req, res) {

    var arg1 = url.parse(req.url).query;
    var arg = url.parse(req.url, true).query;
    console.log(arg);

    var ppkUri = arg['ppk-uri'];
    /**
     * code block
     * 
     * 1. 获取ppk-uri应先check本地资源是否存在,存在则直接返回资源文件;
     * 2. 不存在则需要需要请求比特币区块链获取ODIN标识,然后解析入口参数,再次转发请求以便获取资源文件;
     * ps: 由于时间和网络速率原因,此处代码未实现, 以下为模拟场景.
     */
    console.log(ppkUri);
    console.log();

    request('http://192.168.2.22:3000/?'+arg1, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body) // 打印内容主体日志
        if('jsonp'==arg.type){
            res.end('callback2('+body+')'); // 支持跨域返回
        }else{
            res.end(body);
        }
      }
    })
}).listen(3001);